module gitlab.com/czerasz/lambda-cloudwatch-logs-to-es

go 1.15

require (
	github.com/aws/aws-lambda-go v1.20.0
	github.com/aws/aws-sdk-go v1.35.28
	gitlab.com/czerasz/go-aws-log-parsers v0.0.0-20201115191415-b0fbc612a514 // indirect
	gitlab.com/czerasz/go-utils v0.1.0
)
