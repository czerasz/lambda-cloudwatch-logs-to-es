package es_index_name_test

import (
	"fmt"
	"os"
	"testing"
	"time"

	indexName "gitlab.com/czerasz/lambda-cloudwatch-logs-to-es/es-index-name"
)

func TestIndexName(t *testing.T) {
	date := time.Date(2020, 11, 1, 0, 0, 0, 0, time.UTC)
	tests := []struct {
		name    string
		want    string
		wantErr bool
	}{
		{
			name:    "simple",
			want:    fmt.Sprintf("cloudwatch-%d.%02d.%02d", date.Year(), date.Month(), date.Day()),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := indexName.Generate(date)
			if (err != nil) != tt.wantErr {
				t.Errorf("IndexName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("IndexName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIndexNameWithTemplate(t *testing.T) {
	os.Setenv(indexName.IdxVarName, "{{ .Env.PREFIX }}-{{ .Date.Year }}.{{ .Date.Month }}.{{ .Date.Day }}")
	os.Setenv("PREFIX", "test")
	defer func() {
		os.Unsetenv(indexName.IdxVarName)
		os.Unsetenv("PREFIX")
	}()

	date := time.Date(2020, 11, 1, 0, 0, 0, 0, time.UTC)

	tests := []struct {
		name    string
		want    string
		wantErr bool
	}{
		{
			name:    "custom index template",
			want:    fmt.Sprintf("test-%d.%02d.%02d", date.Year(), date.Month(), date.Day()),
			wantErr: false,
		},
		{
			name:    "invalid index template",
			want:    fmt.Sprintf("test-%d.%02d.%02d", date.Year(), date.Month(), date.Day()),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := indexName.Generate(date)
			if (err != nil) != tt.wantErr {
				t.Errorf("IndexName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("IndexName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIndexNameWithInvalidTemplate(t *testing.T) {
	os.Setenv(indexName.IdxVarName, "{{ Env }}-{{ .Date.Year }}.{{ .Date.Month }}.{{ .Date.Day }}")
	defer func() {
		os.Unsetenv(indexName.IdxVarName)
	}()

	date := time.Date(2020, 11, 1, 0, 0, 0, 0, time.UTC)

	tests := []struct {
		name    string
		want    string
		wantErr bool
	}{
		{
			name:    "invalid index template",
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := indexName.Generate(date)
			if (err != nil) != tt.wantErr {
				t.Errorf("IndexName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("IndexName() = %v, want %v", got, tt.want)
			}
		})
	}
}
