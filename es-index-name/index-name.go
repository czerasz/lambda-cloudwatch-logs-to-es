package es_index_name

import (
	"bytes"
	"fmt"
	"strconv"
	"text/template"
	"time"

	"gitlab.com/czerasz/go-utils/env"
)

// IdxVarName is the name of the environment variable
// which is used to overwrite the default
// ElasticSearch index name template
const IdxVarName = "ES_INDEX_TEMPLATE"
const defaultIdxTpl = "{{ .Prefix }}-{{ .Date.Year }}.{{ .Date.Month }}.{{ .Date.Day }}"

type tmpDate struct {
	Year  string
	Month string
	Day   string
}

type tmpData struct {
	Env    map[string]string
	Date   tmpDate
	Prefix string
}

// Generate is generating the index name
func Generate(date time.Time) (string, error) {
	t := template.New("index")
	tpl, err := t.Parse(env.GetEnv(IdxVarName, defaultIdxTpl))
	if err != nil {
		return "", err
	}

	allEnvVars := env.AllVars()
	// Do NOT include the ES_INDEX_TEMPLATE
	// environment variable
	delete(allEnvVars, IdxVarName)

	data := tmpData{
		Date: tmpDate{
			Year:  strconv.Itoa(date.Year()),
			Month: fmt.Sprintf("%02d", date.Month()),
			Day:   fmt.Sprintf("%02d", date.Day()),
		},
		Prefix: "cloudwatch",
		Env:    allEnvVars,
	}

	var output bytes.Buffer
	if err := tpl.Execute(&output, data); err != nil {
		return "", err
	}

	return output.String(), nil
}
