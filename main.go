package main

import (
	"archive/zip"
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	cf "gitlab.com/czerasz/go-aws-log-parsers/cloudfront"
	env "gitlab.com/czerasz/go-utils/env"
	esIndexName "gitlab.com/czerasz/lambda-cloudwatch-logs-to-es/es-index-name"
)

var debug = os.Getenv("DEBUG") == "true"

func handler(ctx context.Context, e events.S3Event) error {
	if debug {
		// Stringify and output event in debug mode
		str, err := json.Marshal(e)
		if err != nil {
			return err
		}
		fmt.Printf("%s", string(str))
	}

	for _, record := range e.Records {
		path, err := cf.ParseS3Path(record.S3.Object.Key)

		indexName, err := esIndexName.Generate(path.Date)
		if err != nil {
			return err
		}

		fmt.Printf("index name: %s\n", indexName)

		// The session the S3 Downloader will use
		sess, err := session.NewSession(&aws.Config{Region: aws.String(env.GetEnv("AWS_S3_REGION", "eu-central-1"))})
		if err != nil {
			return err
		}

		// Create a downloader with the session and default options
		downloader := s3manager.NewDownloader(sess)

		tmp, err := ioutil.TempFile(os.TempDir(), "cloudfront")
		if err != nil {
			return err
		}
		defer os.Remove(tmp.Name())

		// Create a file to write the S3 Object contents to.
		f, err := os.Create(tmp.Name())
		if err != nil {
			return fmt.Errorf("failed to create file %q, %v", tmp.Name(), err)
		}
		defer f.Close()

		// Write the contents of S3 Object to the file
		n, err := downloader.Download(f, &s3.GetObjectInput{
			Bucket: aws.String(record.S3.Bucket.Name),
			Key:    aws.String(record.S3.Object.Key),
		})

		if err != nil {
			return fmt.Errorf("failed to download file (s3://%s%s), %v", record.S3.Bucket.Name, record.S3.Object.Key, err)
		}

		fmt.Printf("file downloaded, %d bytes\n", n)

		paths, err := unzip(tmp.Name())
		fmt.Printf("paths: %+v\n", paths)
		defer (func() {
			for _, path := range paths {
				os.Remove(path)
			}
		})()

		lines := []cf.WebLog{}
		for _, path := range paths {
			logs, err := parseLogFile(path)

			if err != nil {
				return err
			}

			lines = append(lines, logs...)
		}
		fmt.Printf("logs: %+v\n", lines)

		// u := url.URL{
		// 	Scheme: env.GetEnv("ES_SCHEME", "https"),
		// 	Host:   env.GetEnv("ES_HOST", "127.0.0.1"),
		// }

		// // Define basic ElasticSearch client options
		// esOpts := []elastic.ClientOptionFunc{
		// 	elastic.SetURL(u.String()),
		// 	elastic.SetHealthcheck(false),
		// 	elastic.SetSniff(false),
		// }

		// // If the AWS_ES_REGION environment variable is set the requests will be signed
		// if awsEsRegion := os.Getenv("AWS_ES_REGION"); awsEsRegion != "" {
		// 	// session := awsSession.Must(awsSession.NewSession())
		// 	creds := awsCredentials.NewEnvCredentials()

		// 	signer := v4.NewSigner(creds)
		// 	awsClient, clientErr := awsSigningClient.New(signer, nil, awsESSvc.ServiceName, awsEsRegion)

		// 	if clientErr != nil {
		// 		return clientErr
		// 	}

		// 	esOpts = append(esOpts, elastic.SetHttpClient(awsClient))
		// 	esOpts = append(esOpts, elastic.SetScheme("https"))
		// }

		// // Create a client
		// client, err := elastic.NewClient(
		// 	esOpts...,
		// )
		// if err != nil {
		// 	return err
		// }

		// // Add a document to the index
		// _, err = client.Index().
		// 	Index(indexName).
		// 	BodyJson("snsMsg").
		// 	Refresh("wait_for").
		// 	Do(ctx)

		// if err != nil {
		// 	return err
		// }
	}
	return nil
}

func main() {
	// Used for local testing
	if env.GetEnv("LOCAL_TEST", "false") == "true" {
		// Get JSON object from the pipe
		var e events.S3Event
		if err := json.Unmarshal(readPipe(), &e); err != nil {
			panic(err)
		}

		ctx := context.Background()

		// Execute serverless logic
		err := handler(ctx, e)

		if err != nil {
			panic(err)
		}
	} else {
		lambda.Start(handler)
	}
}

// Read data send via the pipe
func readPipe() []byte {
	reader := bufio.NewReader(os.Stdin)
	var output []byte

	for {
		input, err := reader.ReadByte()
		if err != nil && err == io.EOF {
			break
		}
		output = append(output, input)
	}

	return output
}

// Resource: https://golangcode.com/unzip-files-in-go/
// unzip will decompress the zip archive
// it will move the "terraform" file to the specified path
func unzip(src string) (paths []string, err error) {
	r, err := zip.OpenReader(src)
	if err != nil {
		return
	}
	defer r.Close()

	for _, f := range r.File {
		tmp, err := ioutil.TempFile(os.TempDir(), "cloudfront")
		if err != nil {
			return paths, err
		}

		dstFile, err := os.OpenFile(tmp.Name(), os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return paths, err
		}

		rc, err := f.Open()
		if err != nil {
			return paths, err
		}

		_, err = io.Copy(dstFile, rc)

		if err != nil {
			return paths, err
		}

		// Close the file without defer to close before next iteration of loop
		dstFile.Close()
		rc.Close()

		paths = append(paths, tmp.Name())
	}

	return paths, nil
}

func parseLogFile(path string) ([]cf.WebLog, error) {
	logs := []cf.WebLog{}

	file, err := os.Open(path)
	if err != nil {
		return logs, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		log, err := cf.ParseWebLog(line)
		if err != nil {
			return logs, err
		}
		logs = append(logs, *log)
	}

	if err := scanner.Err(); err != nil {
		return logs, err
	}

	return logs, nil
}
